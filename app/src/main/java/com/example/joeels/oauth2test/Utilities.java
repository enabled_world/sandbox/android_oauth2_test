package com.example.joeels.oauth2test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by joeels on 20/10/16.
 */

public final class Utilities {

    private Utilities() {

    }

    public static void alertView(final Activity activity,
                           final String title,
                           final String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void dismissProgressDialog(final ProgressDialog progressDialog) {

        if (null == progressDialog) {
            // ignore
        } else if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public static Boolean extract_boolean(final JSONObject json_object, final String name) throws JSONException {

        if (json_object.has(name)) {
            return json_object.getBoolean(name);

        } else {
            return null;
        }
    }

    public static Long extract_long(final JSONObject json_object, final String name) throws JSONException {

        if (json_object.has(name)) {
            return json_object.getLong(name);

        } else {
            return null;
        }
    }

    public static String extract_string(final JSONObject json_object, final String name) throws JSONException {

        if (json_object.has(name)) {
            return json_object.getString(name);

        } else {
            return null;
        }
    }

    public static String getAndroidId(Activity activity) {

        final String android_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static void setText(final TextView textView,
                               final SharedPreferences preferences,
                               final String key,
                               final long defaultValue) {

        final long value = preferences.getLong(key, defaultValue);
        textView.setText(Long.toString(value));
    }

    public static void setText(final TextView textView,
                               final SharedPreferences preferences,
                               final String key,
                               final String defaultValue) {

        final String value = preferences.getString(key, defaultValue);
        textView.setText(value);
    }

    public static void startActivity(
            final Activity activeActivity,
            final Class<? extends Activity> targetActivityClass) {

        final Intent intent = new Intent(activeActivity, targetActivityClass);
        activeActivity.startActivity(intent);
        activeActivity.finish();
    }

    public static void startActivity(
            final Activity activeActivity,
            final Class<? extends Activity> targetActivityClass,
            final String... parameters) {

        final Intent intent = new Intent(activeActivity, targetActivityClass);
        for (int index = 0; parameters.length > index; index += 2) {
            final int next = index + 1;
            if (parameters.length > next) {
                intent.putExtra(parameters[index], parameters[next]);
            }
        }

        activeActivity.startActivity(intent);
        activeActivity.finish();
    }

    public static void store(final Activity activity,
                             final String error,
                             final String error_description) {

        final SharedPreferences preferences = activity.getSharedPreferences("user_info", 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString("error", error);
        editor.putString("error_description", error_description);

        editor.commit();
    }

    public static void store(final Activity activity,
                             final String access_token,
                             final Long expires_in,
                             final String token_type,
                             final String scope,
                             final String refresh_token) {

        //Calculate date of expiration
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, expires_in.intValue());
        final long expiry_date = calendar.getTimeInMillis();

        ////Store both expires in and access token in shared preferences
        final SharedPreferences preferences = activity.getSharedPreferences("user_info", 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putLong("expiry_date", expiry_date);
        editor.putString("access_token", access_token);
        editor.putLong("expires_in", expires_in);
        editor.putString("token_type", token_type);
        editor.putString("scope", scope);
        editor.putString("refresh_token", refresh_token);

        editor.commit();
    }

    public static void store(final Activity activity,
                             final String client_id,
                             final String client_secret,
                             final String state,
                             final String redirect_uri) {

        final SharedPreferences preferences = activity.getSharedPreferences("user_info", 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString("client_id", client_id);
        editor.putString("client_secret", client_secret);
        editor.putString("state", state);
        editor.putString("redirect_uri", redirect_uri);

        editor.commit();
    }

    public static void store(final Activity activity,
                             final String public_key) {

        final SharedPreferences preferences = activity.getSharedPreferences("user_info", 0);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString("public_key", public_key);

        editor.commit();
    }
}
