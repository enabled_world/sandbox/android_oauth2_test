package com.example.joeels.oauth2test;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

import static com.example.joeels.oauth2test.KeyWords.*;

/**
 * Created by joeels on 20/10/16.
 */

public final class ReachesMeGetAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final Activity activity;
    private final String url;
    private final List<NameValuePair> parameters;
    private volatile ProgressDialog progressDialog;
    private volatile String message;

    public ReachesMeGetAsyncTask(final Activity activity,
                                 final String url) {

        this.activity = activity;
        this.url = url;
        this.parameters = new ArrayList<>();
    }

    public ReachesMeGetAsyncTask addParameter(final String name,
                                              final String value) {

        synchronized (parameters) {
            final NameValuePair parameter = new BasicNameValuePair(name, value);
            parameters.add(parameter);
        }

        return this;
    }

    @Override
    protected void onPreExecute() {
        Log.d("ReachesMeGet", "void onPreExecute()");

        progressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.loading), true);
    }

    private String create_url() {

        final StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(url);
        boolean first = true;
        for (final NameValuePair parameter : parameters) {

            if (first) {
                first = false;
                urlBuilder.append(QUESTION_MARK);
            } else {
                urlBuilder.append(AMPERSAND);
            }
            urlBuilder.append(parameter.getName()).append(EQUALS).append(parameter.getValue());
        }

        return urlBuilder.toString();
    }

    @Override
    protected Boolean doInBackground(Void... arguments) {
        Log.d("ReachesMeGet", "Boolean doInBackground(Void... arguments)");

        final HttpClient httpClient = HttpClients.createDefault();

        final HttpGet httpGet = new HttpGet(create_url());
        try {

            Log.d("ReachesMeGet", "Request - " + httpGet.toString());
            final HttpResponse response = httpClient.execute(httpGet);
            if (response != null) {
                Log.d("ReachesMeGet", "Response - " + response.toString());
                //If status is OK 200
                if (200 == response.getStatusLine().getStatusCode()) {
                    handle_success(response);
                    return true;
                } else {
                    Utilities.store(activity, "Error response", response.toString());
                }
            }
        } catch (IOException e) {
            Log.e("ReachesMeGet", "Error Http response " + e.getLocalizedMessage());
        } catch (ParseException e) {
            Log.e("ReachesMeGet", "Error Parsing Http response " + e.getLocalizedMessage());
        } catch (JSONException e) {
            Log.e("ReachesMeGet", "Error Parsing JSON " + e.getLocalizedMessage());
        }

        return false;
    }

    private void handle_success(final HttpResponse response) throws IOException, JSONException {

        final HttpEntity entity = response.getEntity();
        final InputStream inputStream = entity.getContent();
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream((int) entity.getContentLength());

        final byte[] buffer = new byte[1024];
        for (int read = inputStream.read(buffer); 0 < read; read = inputStream.read(buffer)) {

            outputStream.write(buffer, 0, read);
        }

        final String body = new String(outputStream.toByteArray());
        Log.i("ReachesMeGet", "body=" + body);

        final JSONObject object = new JSONObject(body);
        message = object.toString(3);
    }

    @Override
    protected void onPostExecute(Boolean status) {
        Log.d("ReachesMeGet", "void onPostExecute(Boolean status)");

        Utilities.dismissProgressDialog(progressDialog);
        if (status) {
            Utilities.startActivity(activity, MessageOutputActivity.class, "message", message);
        } else {
            Utilities.startActivity(activity, ErrorActivity.class);
        }
    }
}
