package com.example.joeels.oauth2test;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static com.example.joeels.oauth2test.KeyWords.*;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog progressDialog;
    private Random random;
    private TextView access_token;
    private TextView expires_in;
    private TextView token_type;
    private TextView scope;
    private TextView refresh_token;
    private TextView expiry_date;
    private Button refresh_button;
    private Button validate_button;
    private Button verify_button;
    private Button task1_button;
    private Button task2_button;
    private Button task3_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        progressDialog = ProgressDialog.show(this, "", this.getString(R.string.loading), true);
        try {

            random = new Random(System.currentTimeMillis());

            access_token = (TextView) findViewById(R.id.access_token);
            expires_in = (TextView) findViewById(R.id.expires_in);
            token_type = (TextView) findViewById(R.id.token_type);
            scope = (TextView) findViewById(R.id.scope);
            refresh_token = (TextView) findViewById(R.id.refresh_token);

            expiry_date = (TextView) findViewById(R.id.expiry_date);

            refresh_button = (Button) findViewById(R.id.refresh_button);
            refresh_button.setOnClickListener(this);

            validate_button = (Button) findViewById(R.id.validate_button);
            validate_button.setOnClickListener(this);

            verify_button = (Button) findViewById(R.id.verify_button);
            verify_button.setOnClickListener(this);

            task1_button = (Button) findViewById(R.id.task1_button);
            task1_button.setOnClickListener(this);

            task2_button = (Button) findViewById(R.id.task2_button);
            task2_button.setOnClickListener(this);

            task3_button = (Button) findViewById(R.id.task3_button);
            task3_button.setOnClickListener(this);

            reloadData();
        } finally {
            Utilities.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void onClick(View v) {

        if (refresh_button == v) {
            final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
            final String client_id = preferences.getString("client_id", KeyWords.CLIENT_ID);
            final String client_secret = preferences.getString("client_secret", KeyWords.CLIENT_SECRET);
            // final String state = preferences.getString("state", KeyWords.STATE);
            final String redirect_uri = preferences.getString("redirect_uri", KeyWords.REDIRECT_URI);

            final String refresh_token = preferences.getString("refresh_token", null);
            //Generate URL for requesting Access Token
            //We make the request in a AsyncTask
            new PostAccessTokenAsyncTask(this, ACCESS_TOKEN_URL)
                    .addParameter(GRANT_TYPE_PARAM, "refresh_token")
                    .addParameter("refresh_token", refresh_token)
                    .addParameter(CLIENT_ID_PARAM, client_id)
                    .addParameter(REDIRECT_URI_PARAM, redirect_uri)
                    .addParameter(SECRET_KEY_PARAM, client_secret)
                    .execute();

        } else if (validate_button == v) {
            final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
            final String access_token = preferences.getString("access_token", null);

            new PostValidateAsyncTask(this, RESOURCE_URL)
                    .addParameter("access_token", access_token)
                    .execute();

        } else if (verify_button == v) {
            final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
            final String client_id = preferences.getString("client_id", null);

            new LoadPublicKeyAsyncTask(this, PUBLIC_KEY_URL)
                    .addParameter("client_id", client_id)
                    .execute();

        } else if (task1_button == v) {
            final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
            final String token = preferences.getString("access_token", null);

            final ReachesMeGetAsyncTask task = new ReachesMeGetAsyncTask(this, REACHES_ME_GET_URL);
            task.addParameter("version", "1.0")
                    .addParameter("msgid", Integer.toString(random.nextInt(1000)))
                    .addParameter("type", "resourcerequest")
                    .addParameter("access_token", token);

            task.execute();
        } else {

            Utilities.alertView(this, "Error", "No task associated with this button.");
        }
    }

    private void reloadData() {

        final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
        Utilities.setText(access_token, preferences, "access_token", null);
        Utilities.setText(expires_in, preferences, "expires_in", 0);
        Utilities.setText(token_type, preferences, "token_type", null);
        Utilities.setText(scope, preferences, "scope", null);
        Utilities.setText(refresh_token, preferences, "refresh_token", null);

        final long value = preferences.getLong("expiry_date", 0);
        if (0 == value) {
            expiry_date.setText("(?)");
        } else {
            final Date date = new Date(value);
            final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final String text = dateFormat.format(date);
            expiry_date.setText("(" + text + ")");
        }
    }
}
