package com.example.joeels.oauth2test;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by joeels on 20/10/16.
 */

public final class LoadPublicKeyAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final Activity activity;
    private final String url;
    private final List<NameValuePair> parameters;
    private volatile ProgressDialog progressDialog;

    public LoadPublicKeyAsyncTask(final Activity activity,
                                  final String url) {

        this.activity = activity;
        this.url = url;
        this.parameters = new ArrayList<>();
    }

    public LoadPublicKeyAsyncTask addParameter(final String name,
                                               final String value) {

        synchronized (parameters) {
            final NameValuePair parameter =  new BasicNameValuePair(name, value);
            parameters.add(parameter);
        }

        return this;
    }

    @Override
    protected void onPreExecute() {
        Log.d("PublicKey", "void onPreExecute()");

        progressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.loading), true);
    }

    @Override
    protected Boolean doInBackground(Void... arguments) {
        Log.d("PublicKey", "Boolean doInBackground(Void... arguments)");

        final HttpClient httpClient = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost(url);
        try {
            final HttpEntity entity = new UrlEncodedFormEntity(parameters, HTTP.UTF_8);
            httpPost.setEntity(entity);

            Log.d("PublicKey", "Request - " + httpPost.toString());
            final HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                Log.d("PublicKey", "Response - " + response.toString());
                //If status is OK 200
                if (200 == response.getStatusLine().getStatusCode()) {
                    handle_success(response);
                    return true;
                } else {
                    Utilities.store(activity, "Error response", response.toString());
                }
            }
        } catch (IOException e) {
            Log.e("PublicKey", "Error Http response " + e.getLocalizedMessage());
        } catch (ParseException e) {
            Log.e("PublicKey", "Error Parsing Http response " + e.getLocalizedMessage());
        }

        return false;
    }

    private void handle_success(final HttpResponse response) throws IOException {

        final HttpEntity entity = response.getEntity();
        final InputStream inputStream = entity.getContent();
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        final byte[] buffer = new byte[1024];
        for (int read = inputStream.read(buffer); 0 < read; read = inputStream.read(buffer)) {

            outputStream.write(buffer, 0, read);
        }

        final String public_key = new String(outputStream.toByteArray());
        Log.i("PublicKey", "public_key=" + public_key);
        Utilities.store(activity, public_key);
    }

    @Override
    protected void onPostExecute(Boolean status) {
        Log.d("PublicKey", "void onPostExecute(Boolean status)");

        Utilities.dismissProgressDialog(progressDialog);
        if (status) {
            Utilities.startActivity(activity, VerifyActivity.class);
        } else {
            Utilities.startActivity(activity, ErrorActivity.class);
        }
    }
}
