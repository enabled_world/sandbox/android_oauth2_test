package com.example.joeels.oauth2test;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ErrorActivity extends AppCompatActivity {

    private TextView error;
    private TextView error_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        error = (TextView) findViewById(R.id.error);
        error_description = (TextView) findViewById(R.id.error_description);

        final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
        Utilities.setText(error, preferences, "error", null);
        Utilities.setText(error_description, preferences, "error_description", null);
    }
}
