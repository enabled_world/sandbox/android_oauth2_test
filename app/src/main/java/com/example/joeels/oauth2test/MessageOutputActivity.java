package com.example.joeels.oauth2test;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MessageOutputActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog progressDialog;
    private TextView message;
    private Button ok_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_output);

        progressDialog = ProgressDialog.show(this, "", this.getString(R.string.loading), true);
        try {
            message = (TextView) findViewById(R.id.message);

            ok_button = (Button) findViewById(R.id.ok_button);
            ok_button.setOnClickListener(this);

            final Bundle extras = getIntent().getExtras();
            if (null != extras) {
                final String msg = extras.getString("message");
                message.setText(msg);
            }

        } finally {
            Utilities.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void onClick(View v) {

        if (ok_button == v) {
            Utilities.startActivity(this, SuccessActivity.class);
        }
    }
}
