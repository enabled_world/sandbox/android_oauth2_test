package com.example.joeels.oauth2test;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import static com.example.joeels.oauth2test.KeyWords.*;

public class WebClientActivity extends AppCompatActivity {

    private WebView webView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("WebClientActivity", "void onCreate(Bundle savedInstanceState)");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_client);

        final CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        //get the webView from the layout
        webView = (WebView) findViewById(R.id.main_activity_web_view);
        final WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        //Request focus for the webview
        webView.requestFocus(View.FOCUS_DOWN);

        //Show a progress dialog to the user
        progressDialog = ProgressDialog.show(this, "", this.getString(R.string.loading), true);

        //Set a custom web view client
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("AuthorizationCode", "void onPageFinished(WebView view, String url)");

                //This method will be executed each time a page finished loading.
                //The only we do is dismiss the progressDialog, in case we are showing any.
                Utilities.dismissProgressDialog(progressDialog);
            }

            private boolean shouldOverride(final Uri uri) {

                final String url = uri.toString();

                final SharedPreferences preferences = WebClientActivity.this.getSharedPreferences("user_info", 0);
                final String redirect_uri = preferences.getString("redirect_uri", KeyWords.REDIRECT_URI);

                //This method will be called when the Auth proccess redirect to our RedirectUri.
                //We will check the url looking for our RedirectUri.
                if (url.startsWith(redirect_uri)) {
                    overrideUrl(uri);

                } else {
                    //Default behaviour
                    Log.i("Authorize", "Redirecting to: " + url);
                    webView.loadUrl(url);
                }
                return true;
            }

            @Override
            @TargetApi(16)
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("AuthorizationCode", "boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)");

                final Uri uri = Uri.parse(url);
                return shouldOverride(uri);
            }

            @Override
            @TargetApi(21)
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                Log.d("AuthorizationCode", "boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)");

                final Uri uri = request.getUrl();
                return shouldOverride(uri);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                Log.e("Authorize", "There is an issue with the SSL certificate!");
                Log.e("Authorize", "View: " + view);
                Log.e("Authorize", "Handler: " + handler);
                Log.e("Authorize", "Error: " + error);
                super.onReceivedSslError(view, handler, error);
            }
        });

        //Get the authorization Url
        String authUrl = getAuthorizationUrl();
        Log.i("Authorize", "Loading Auth Url: " + authUrl);
        //Load the authorization URL into the webView
        webView.loadUrl(authUrl);
    }

    private void overrideUrl(final Uri uri) {

        //We take from the url the authorizationToken and the state token. We have to check that the state token returned by the Service is the same we sent.
        //If not, that means the request may be a result of CSRF and must be rejected.
        final String state = uri.getQueryParameter(STATE_PARAM);
        if (!Utilities.getAndroidId(this).equals(state)) {
            Log.e("AuthorizationCode", "State token doesn't match");
        } else {

            //If the user doesn't allow authorization to our application, the authorizationToken Will be null.
            final String authorization_code = uri.getQueryParameter(RESPONSE_TYPE_VALUE);
            if (null != authorization_code) {
                Log.i("AuthorizationCode", "authorization_code received: " + authorization_code);

                final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
                final String client_id = preferences.getString("client_id", KeyWords.CLIENT_ID);
                final String client_secret = preferences.getString("client_secret", KeyWords.CLIENT_SECRET);
                // final String state = preferences.getString("state", KeyWords.STATE);
                final String redirect_uri = preferences.getString("redirect_uri", KeyWords.REDIRECT_URI);

                //We make the request in a AsyncTask
                new PostAccessTokenAsyncTask(this, ACCESS_TOKEN_URL)
                        .addParameter(GRANT_TYPE_PARAM, GRANT_TYPE)
                        .addParameter(RESPONSE_TYPE_VALUE, authorization_code)
                        .addParameter(CLIENT_ID_PARAM, client_id)
                        .addParameter(REDIRECT_URI_PARAM, redirect_uri)
                        .addParameter(SECRET_KEY_PARAM, client_secret)
                        .execute();

            } else {
                final String error = uri.getQueryParameter("error");
                final String error_description = uri.getQueryParameter("error_description");

                Log.e("AuthorizationCode", "error=" + error + ", error_description=" + error_description);
                Utilities.store(WebClientActivity.this, error, error_description);
                Utilities.startActivity(WebClientActivity.this, ErrorActivity.class);
            }
        }
    }

    /**
     * Method that generates the url for get the authorization token from the Service
     *
     * @return Url
     */
    private String getAuthorizationUrl() {

        final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
        final String client_id = preferences.getString("client_id", KeyWords.CLIENT_ID);
        final String state = preferences.getString("state", Utilities.getAndroidId(this));
        final String redirect_uri = preferences.getString("redirect_uri", KeyWords.REDIRECT_URI);

        return AUTHORIZATION_URL
                + QUESTION_MARK + RESPONSE_TYPE_PARAM + EQUALS + RESPONSE_TYPE_VALUE
                + AMPERSAND + CLIENT_ID_PARAM + EQUALS + client_id
                + AMPERSAND + STATE_PARAM + EQUALS + state
                + AMPERSAND + REDIRECT_URI_PARAM + EQUALS + redirect_uri;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("WebClientActivity", "boolean onCreateOptionsMenu(Menu menu)");

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
