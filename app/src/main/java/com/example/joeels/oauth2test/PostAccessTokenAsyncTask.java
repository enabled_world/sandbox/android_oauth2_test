package com.example.joeels.oauth2test;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by joeels on 20/10/16.
 */

public final class PostAccessTokenAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final Activity activity;
    private final String url;
    private final List<NameValuePair> parameters;
    private volatile ProgressDialog progressDialog;

    public PostAccessTokenAsyncTask(final Activity activity,
             final String url) {

        this.activity = activity;
        this.url = url;
        this.parameters = new ArrayList<>();
    }

    public PostAccessTokenAsyncTask addParameter(final String name,
                                                 final String value) {

        synchronized (parameters) {
            final NameValuePair parameter =  new BasicNameValuePair(name, value);
            parameters.add(parameter);
        }

        return this;
    }

    @Override
    protected void onPreExecute() {
        Log.d("AccessToken", "void onPreExecute()");

        progressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.loading), true);
    }

    @Override
    protected Boolean doInBackground(Void... arguments) {
        Log.d("AccessToken", "Boolean doInBackground(Void... arguments)");

        final HttpClient httpClient = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost(url);
        try {
            final HttpEntity entity = new UrlEncodedFormEntity(parameters, HTTP.UTF_8);
            httpPost.setEntity(entity);

            Log.d("AccessToken", "Request - " + httpPost.toString());
            final HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                Log.d("AccessToken", "Response - " + response.toString());
                //If status is OK 200
                if (200 == response.getStatusLine().getStatusCode()) {
                    return handle_success(response);
                } else {
                    handle_error(response);
                }
            }
        } catch (IOException e) {
            Log.e("AccessToken", "Error Http response " + e.getLocalizedMessage());
        } catch (ParseException e) {
            Log.e("AccessToken", "Error Parsing Http response " + e.getLocalizedMessage());
        } catch (JSONException e) {
            Log.e("AccessToken", "Error Parsing Http response " + e.getLocalizedMessage());
        }

        return false;
    }

    private JSONObject extract_json_object(final HttpResponse response) throws IOException, JSONException {

        final String result = EntityUtils.toString(response.getEntity());
        final JSONObject json_object = new JSONObject(result);

        return json_object;
    }

    private void handle_error(final HttpResponse response) throws IOException, JSONException {

        final JSONObject json_object = extract_json_object(response);
        if (null != json_object) {
            handle_error(json_object);
        }
    }

    private void handle_error(final JSONObject response) throws IOException, JSONException {

        final String error = Utilities.extract_string(response, "error");
        final String error_description = Utilities.extract_string(response, "error_description");

        Log.e("AccessToken", "error=" + error + ", error_description=" + error_description);
        Utilities.store(activity, error, error_description);
    }

    private boolean handle_success(final HttpResponse response) throws IOException, JSONException {

        final JSONObject json_object = extract_json_object(response);
        return handle_success(json_object);
    }

    private boolean handle_success(final JSONObject response) throws IOException, JSONException {

        final String access_token = Utilities.extract_string(response, "access_token");
        final Long expires_in = Utilities.extract_long(response, "expires_in");
        final String token_type = Utilities.extract_string(response, "token_type");
        final String scope = Utilities.extract_string(response, "scope");
        final String refresh_token = Utilities.extract_string(response, "refresh_token");

        return handle_success(access_token, expires_in, token_type, scope, refresh_token);
    }

    private boolean handle_success(final String access_token,
                                   final Long expires_in,
                                   final String token_type,
                                   final String scope,
                                   final String refresh_token) {

        // {"access_token":"700fa3769e7d942276e4ec76f6d5f08febe40c60","expires_in":3600,"token_type":"Bearer","scope":null,"refresh_token":"811aa70f859e7449a25a2f97ca5bf0991a981665"}
        Log.d("AccessToken", "access_token=" + access_token + ", expires_in=" + expires_in + ", token_type=" + token_type + ", scope=" + scope + ", refresh_token=" + refresh_token);

        final boolean handle = (null != access_token) && (null != expires_in);
        if (handle) {
            Utilities.store(activity, access_token, expires_in, token_type, scope, refresh_token);
        }

        return handle;
    }


    @Override
    protected void onPostExecute(Boolean status) {
        Log.d("AccessToken", "void onPostExecute(Boolean status)");

        Utilities.dismissProgressDialog(progressDialog);
        if (status) {
            Utilities.startActivity(activity, SuccessActivity.class);
        } else {
            Utilities.startActivity(activity, ErrorActivity.class);
        }
    }
}
