package com.example.joeels.oauth2test;

/**
 * Created by joeels on 20/10/16.
 */

public final class KeyWords {
/*CONSTANT FOR THE AUTHORIZATION PROCESS*/

    //This is the public api key of our application
    public static final String CLIENT_ID = "testclient"; // "YOUR_API_KEY";
    //This is the private api key of our application
    public static final String CLIENT_SECRET = "testpass"; // "YOUR_API_SECRET";
    //This is any string we want to use. This will be used for avoiding CSRF attacks. You can generate one here: http://strongpasswordgenerator.com/
    //public static final String STATE = "E3ZYKC1T6H2yP4z";
    //This is the url that LinkedIn Auth process will redirect to. We can put whatever we want that starts with http:// or https:// .
//We use a made up url that we will intercept when redirecting. Avoid Uppercases.
    public static final String REDIRECT_URI = "http://localhost.world/oauth2";
    /*********************************************/

//These are constants used for build the urls
    public static final String AUTHORIZATION_URL = "https://oauth2.enabled.world/authorize.php";
    public static final String ACCESS_TOKEN_URL = "https://oauth2.enabled.world/token.php";
    public static final String RESOURCE_URL = "https://oauth2.enabled.world/resource.php";
    public static final String PUBLIC_KEY_URL = "https://oauth2.enabled.world/public_key.php";
    public static final String REACHES_ME_GET_URL = "https://reaches.me/get";
    public static final String SECRET_KEY_PARAM = "client_secret";
    public static final String RESPONSE_TYPE_PARAM = "response_type";
    public static final String GRANT_TYPE_PARAM = "grant_type";
    public static final String GRANT_TYPE = "authorization_code";
    public static final String RESPONSE_TYPE_VALUE = "code";
    public static final String CLIENT_ID_PARAM = "client_id";
    public static final String STATE_PARAM = "state";
    public static final String REDIRECT_URI_PARAM = "redirect_uri";
    /*---------------------------------------*/
    public static final String QUESTION_MARK = "?";
    public static final String AMPERSAND = "&";
    public static final String EQUALS = "=";

    private KeyWords() {

    }
}
