package com.example.joeels.oauth2test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.message.BasicNameValuePair;
import cz.msebera.android.httpclient.protocol.HTTP;
import cz.msebera.android.httpclient.util.EntityUtils;

/**
 * Created by joeels on 20/10/16.
 */

public final class PostValidateAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final Activity activity;
    private final String url;
    private final List<NameValuePair> parameters;
    private volatile ProgressDialog progressDialog;
    private volatile String message;

    public PostValidateAsyncTask(final Activity activity,
                                 final String url) {

        this.activity = activity;
        this.url = url;
        this.parameters = new ArrayList<>();
    }

    public PostValidateAsyncTask addParameter(final String name,
                                              final String value) {

        synchronized (parameters) {
            final NameValuePair parameter = new BasicNameValuePair(name, value);
            parameters.add(parameter);
        }

        return this;
    }

    @Override
    protected void onPreExecute() {
        Log.d("Validate", "void onPreExecute()");

        progressDialog = ProgressDialog.show(activity, "", activity.getString(R.string.loading), true);
    }

    @Override
    protected Boolean doInBackground(Void... arguments) {
        Log.d("Validate", "Boolean doInBackground(Void... arguments)");

        final HttpClient httpClient = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost(url);
        try {
            final HttpEntity entity = new UrlEncodedFormEntity(parameters, HTTP.UTF_8);
            httpPost.setEntity(entity);

            Log.d("Validate", "Request - " + httpPost.toString());
            final HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                Log.d("Validate", "Response - " + response.toString());
                //If status is OK 200
                if (200 == response.getStatusLine().getStatusCode()) {
                    return handle_success(response);
                } else {
                    handle_error(response);
                }
            }
        } catch (IOException e) {
            Log.e("Validate", "Error Http response " + e.getLocalizedMessage());
        } catch (ParseException e) {
            Log.e("Validate", "Error Parsing Http response " + e.getLocalizedMessage());
        } catch (JSONException e) {
            Log.e("Validate", "Error Parsing Http response " + e.getLocalizedMessage());
        }

        return false;
    }

    private JSONObject extract_json_object(final HttpResponse response) throws IOException, JSONException {

        final String result = EntityUtils.toString(response.getEntity());
        final JSONObject json_object = new JSONObject(result);

        return json_object;
    }

    private void handle_error(final HttpResponse response) throws IOException, JSONException {

        final JSONObject json_object = extract_json_object(response);
        if (null != json_object) {
            handle_error(json_object);
        }
    }

    private void handle_error(final JSONObject response) throws IOException, JSONException {

        final String error = Utilities.extract_string(response, "error");
        final String error_description = Utilities.extract_string(response, "error_description");

        Log.e("Validate", "error=" + error + ", error_description=" + error_description);
        Utilities.store(activity, error, error_description);
    }

    private boolean handle_success(final HttpResponse response) throws IOException, JSONException {

        final JSONObject json_object = extract_json_object(response);
        return handle_success(json_object);
    }

    private boolean handle_success(final JSONObject response) throws IOException, JSONException {

        final Boolean success = Utilities.extract_boolean(response, "success");
        final String message = Utilities.extract_string(response, "message");

        this.message = message;

        return (null != success) && success;
    }

    @Override
    protected void onPostExecute(Boolean status) {
        Log.d("Validate", "void onPostExecute(Boolean status)");

        Utilities.dismissProgressDialog(progressDialog);
        if (status) {
            Utilities.alertView(activity, "Validate", message);
        } else {
            Utilities.startActivity(activity, ErrorActivity.class);
        }
    }
}
