package com.example.joeels.oauth2test;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class VerifyActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog progressDialog;
    private TextView jwt_header;
    private TextView jwt_claims;
    private TextView jwt_signature;
    private Button ok_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        progressDialog = ProgressDialog.show(this, "", this.getString(R.string.loading), true);
        try {

            jwt_header = (TextView) findViewById(R.id.jwt_header);
            jwt_claims = (TextView) findViewById(R.id.jwt_claims);
            jwt_signature = (TextView) findViewById(R.id.jwt_signature);

            ok_button = (Button) findViewById(R.id.ok_button);
            ok_button.setOnClickListener(this);

            final SharedPreferences preferences = this.getSharedPreferences("user_info", 0);
            final String access_token = preferences.getString("access_token", null);
            final String public_key = preferences.getString("public_key", null);

            decode(access_token, public_key);
        } finally {
            Utilities.dismissProgressDialog(progressDialog);
        }
    }

    @Override
    public void onClick(View v) {

        if (ok_button == v) {
            Utilities.startActivity(this, SuccessActivity.class);
        }
    }

    private static PublicKey get_public_key(String text) throws NoSuchAlgorithmException, InvalidKeySpecException {

        text = strip_tags(text);
        final byte[] data = Base64.decode(text, Base64.DEFAULT);

        final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        final X509EncodedKeySpec spec = new X509EncodedKeySpec(data);

        return keyFactory.generatePublic(spec);
    }

    private static String strip_tags(final String source) {

        final StringBuilder stringBuilder = new StringBuilder();
        for (final String line : source.split("\n")) {
            if (line.startsWith("--")) {
                // ignore
            } else {
                stringBuilder.append(line).append("\n");
            }
        }

        return stringBuilder.toString();
    }

    private void decode(final String access_token,
                        final String public_key) {

        final String[] segments = access_token.split("\\.");
        if ((null == segments) || (3 != segments.length)) {
            Log.e("Verify", "Invalid JWT access token!");
            jwt_header.setText("Invalide token");
            jwt_claims.setText("Invalide token");
            jwt_signature.setText("Invalide token");

            return;
        }

        // final String header = segments[0];
        // final String claims = segments[1];
        // final String signature = segments[2];

        try {
            final JSONObject header = get_json(segments[0]);
            final JSONObject claims = get_json(segments[1]);

            final String data = segments[0] + "." + segments[1];
            final byte[] signature_data = Base64.decode(segments[2], Base64.URL_SAFE);

            jwt_header.setText(header.toString(3));
            jwt_claims.setText(claims.toString(3));

            final PublicKey key = get_public_key(public_key);
            final Signature signature = get_signature(header.getString("alg"));
            signature.initVerify(key);
            signature.update(data.getBytes());

            if (signature.verify(signature_data)) {
                jwt_signature.setText("Verified");
            } else {
                jwt_signature.setText("Unverified");
            }

        } catch (Exception e) {
            Log.e("Verify", "void decode(final String access_token, final byte[] public_key)", e);

            Utilities.store(this, e.getClass().getSimpleName(), e.getMessage());
            Utilities.startActivity(this, ErrorActivity.class);
        }
    }

    private static JSONObject get_json(final String base64encoded) throws JSONException {

        final byte[] data  = Base64.decode(base64encoded, Base64.URL_SAFE);
        final String raw = new String(data);

        return new JSONObject(raw);
    }

    private static Signature get_signature(String algorithm) throws NoSuchAlgorithmException {

        if("RS256".equals(algorithm)){
            algorithm = "SHA256withRSA";
        } else if("RS515".equals(algorithm)){
            algorithm = "SHA512withRSA";
        } else if("RS384".equals(algorithm)){
            algorithm = "SHA384withRSA";
        } else {
            // by default
            algorithm = "SHA256withRSA";
        }

        return Signature.getInstance(algorithm);
    }
}
